"use strict";

(function(){
  angular
    .module('bd4bf')
    .factory("apiStatistics", apiStatistics);

  apiStatistics.$inject = ['$resource'];

  function apiStatistics($resource){
    return $resource("/bd4bf-api/:method");
  }

})();
