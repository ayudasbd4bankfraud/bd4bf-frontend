"use strict";

(function(){
  angular
    .module('bd4bf')
    .filter("translate", translate);

  function translate(){
  	return function(input) {
		switch(input){
			case "date": return "Fecha";
			case "filename": return "Archivo";
			case "all": return "Total";
			case "fraud": return "Fraudulento";
			case "legal": return "Legal";
			case "count": return "Número";
			case "meanAmount": return "Media";
			case "stdevAmount": return "Desviación típica";
			case "sumAmount": return "Suma";
			case "areaUnderRoc": return "AROC";
			case "specificity" : return "Especificidad";
			case "sensitivity": return "Sensibilidad";
			case "amount": return "Importe";
			case "amountMerchantTypeOverMonth": return "Importe mensual por tipo de comercio";
			case "amountMerchantTypeOverThreeMonths": return "Importe trimestral por tipo de comercio";
			case "amountOverMonth": return "Importe por mes";
			case "amountSameCountryOverMonth": return "Importe mendual por país";
			case "amountSameDay": return "Total por número de día";
			case "averageDailyOverMonth": return "Media diaria mensual";
			case "averageOverThreeMonths": return "Media diaria trimestral";
			case "ecommerce": return "Transacción en ecommerce";
			case "foreign": return "Transacción con moneda extranjera";
			case "numberMerchantTypeOverMonth": return "Tipo de comercio por mes";
			case "numberSameCountryOverMonth": return "País por mes";
			case "numberSameDay": return "Número de día";
			default: return input;
		}
	};
  }
})();
