"use strict";

(function(){
  angular
    .module('bd4bf')
    .controller("trainingCtrl", reportsOverviewCtrl);

  reportsOverviewCtrl.$inject = ['statisticsService', '$timeout', '$filter', '$state', 'MERCHANT_CODE', 'COUNTRY_CODE'];

  function reportsOverviewCtrl(statisticsService, $timeout, $filter, $state, MERCHANT_CODE, COUNTRY_CODE){
    var vm = this;

    var reportsList = [];

    vm.error = false;
    vm.reports = false;
    vm.searchName = searchName;
    vm.startTraining = startTraining;
    vm.trainingDelay = false;
    vm.trainingSuccess = false;
    vm.visible = false;

    init();

    function init(){
      loadReportList();
    }

    function loadReportList(){
      statisticsService.getModelFeaturesList()
        .then(function(list){
          reportsList = list;
          vm.error = false;
          vm.reports = _processModelFeaturesList(list);
        })
        .catch(function(response){
          vm.error = response;
        });
    }

    function _processModelFeaturesList(data){
      var table = {
        name: "",
        references: [],
        callback: _setVisible,
        names: [],
        titles: [],
        values: []
      };

      table.titles = Object.keys(data[0])
        .filter(function(title){
          return title !== "weights";
        });

      data
        .sort(function(a, b){
          return b.date - a.date;
        })
        .forEach(function(row, index){
          var values = [];
          table.names.push("#" + index);
          table.references.push(row['date']);
          table.titles.forEach(function(title){
            if(title === "date"){
              values.push($filter('date')(row[title], 'yyyy-MM-dd HH:mm:ss'));
            }else{
              if(row[title].count instanceof Object){
                values.push(row[title].count.value);
              }else{
                values.push(row[title]);
              }
            }
          });
          table.values.push(values);
        });

      return table;
    }

    function _setVisible(date){
      vm.visible = reportsList
        .filter(function(report){
          return report.date === date;
        })[0];
      // Object.keys(vm.visible.weights).forEach(function(attr){
      //   if(vm.visible.weights[attr].value){
      //     console.log(111)
      //     vm.visible.weights[attr] = vm.visible.weights[attr].value;
      //   }else{
      //     vm.visible.weights[attr] = vm.visible.weights[attr];
      //   }
      // });
    }

    function searchName(name){
      return MERCHANT_CODE(COUNTRY_CODE(name));
    }

    function startTraining(){
      vm.trainingDelay = true;
      statisticsService.getStartTraining()
        .then(function(list){
          vm.trainingSuccess = true;

          $timeout(function(){
            vm.trainingDelay = false;
          }, 5 * 1000);
        });
    }

  }

})();
