"use strict";

(function(){
  angular
    .module('bd4bf')
    .controller("reportsOverviewCtrl", reportsOverviewCtrl);

  reportsOverviewCtrl.$inject = ['$scope', '$timeout', 'statisticsService', 'Upload', '$filter', '$state'];

  function reportsOverviewCtrl($scope, $timeout, statisticsService, Upload, $filter, $state){
    var vm = this;

    vm.error = false;
    vm.reports = false;
    vm.files = [];
    vm.loading = false;

    init();

    function init(){
      loadReportList();

      $scope.$watch('files', function () {
        _upload($scope.files);
      });
    }

    function loadReportList(){
      statisticsService.getStatisticsList()
        .then(function(list){
          vm.error = false;
          vm.reports = _processReportList(list);
        })
        .catch(function(response){
          vm.error = response;
        });
    }

    function _processReportList(data){
      var table = {
        name: "Reportes",
        references: [],
        callback: _goToReport,
        names: [],
        titles: [],
        values: []
      };

      table.titles = Object.keys(data[0])
        .sort(function(a, b){
          var priority = ['fraud','legal','all','date','filename'];
          if(b !== a){
            return priority.indexOf(b) - priority.indexOf(a);
          }
          return 0;
        })
        .filter(function(title){
          return title !== "filename";
        });

      data
        .sort(function(a, b){
          return b.date - a.date;
        })
        .forEach(function(row, index){
          var values = [];
          table.names.push(row['filename']);
          table.references.push(row['date']);
          table.titles.forEach(function(title){
            if(title === "date"){
              values.push($filter('date')(row[title], 'yyyy-MM-dd HH:mm:ss'));
            }else{
              if(row[title].count instanceof Object){
                values.push(row[title].count.value);
              }else{
                values.push(row[title]);
              }
            }
          });
          table.values.push(values);
        });

      return table;
    }

    function _goToReport(date){
      $state.go('reportDetails', {id: date});
    }

    function _upload(files){
      if (files) {
        var files = [files];
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          if (!file.$error) {
            Upload.upload({
              url: '/bd4bf-api/upload',
              data: {
                file: file  
              }
            }).then(function (resp) {
              console.warn(resp.data);
            }, null, function (evt) {
              var progressPercentage = 100.0 * evt.loaded / evt.total;

              if(progressPercentage === 100){
                $timeout(function(){
                  vm.loading = false;
                }, 400);
              }
              vm.loading = progressPercentage;
            });
          }
        }
      }
    };

  }

})();
