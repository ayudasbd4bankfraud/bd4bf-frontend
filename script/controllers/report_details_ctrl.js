"use strict";

(function(){
  angular
    .module('bd4bf')
    .controller("reportDetailsCtrl", reportDetailsCtrl);

  reportDetailsCtrl.$inject = ['statisticsService', '$state', '$filter', 'MERCHANT_CODE', 'COUNTRY_CODE'];

  function reportDetailsCtrl(statisticsService, $state, $filter, MERCHANT_CODE, COUNTRY_CODE){
    var vm = this;

    var types = {
      "all": "Total",
      "legal": "Legal",
      "fraud": "Fraudulento"
    }

    vm.error = false;
    vm.report = null;
    vm.reportType = "amount";
    vm.stackConfiguration = {
      "amount": {scaleLabel: "<%= Number(value / 1000000).toFixed(1).replace('.', ',') + ' M$' %>"},
      "count": {scaleLabel: "<%= value %>"}
    };
    vm.statistics = {
      global: {},
      count: {
        amount: {data: [], labels: []},
        count: {data: [], labels: []}
      },
      amountPerCountry: {
        amount: {data: [], labels: [], series: []},
        count: {data: [], labels: [], series: []}
      },
      amountPerMerchant: {
        amount: {data: [], labels: [], series: []},
        count: {data: [], labels: [], series: []}
      },
      ecommerce: {
        amount: {data: [], labels: [], series: []},
        count: {data: [], labels: [], series: []}
      },
      foreing: {
        amount: {data: [], labels: [], series: []},
        count: {data: [], labels: [], series: []}
      }
    };

    init();

    function init(){
      _loadReport($state.params.id);
    }

    function _loadReport(id){
      statisticsService.getStatistic(id)
        .then(function(report){
          vm.error = false;
          vm.report = report;
          _processStatistics();
        })
        .catch(function(response){
          vm.error = response;
        });
    }

    function _processStatistics(){
      _processStatisticsGlobal(vm.report, vm.statistics.global);
      _processStatisticsCount(vm.report, vm.statistics.count);
      _processStatisticsEcommerce(vm.report, vm.statistics.ecommerce);
      _processStatisticsForeing(vm.report, vm.statistics.foreing);
      _processStatisticsAmountPerCountry(vm.report, vm.statistics.amountPerCountry);
      _processStatisticsAmountPerMerchant(vm.report, vm.statistics.amountPerMerchant);
    }

    function _processStatisticsGlobal(report, statistic){
      var keys = Object.keys(types);
      var values = ["count", "meanAmount", "stdevAmount", "sumAmount"];
      
      statistic.names = [];
      statistic.titles = values;
      statistic.values = [];

      keys.forEach(function(key){
        statistic.names.push(types[key]);
        statistic.values.push(statistic.titles.map(function(title){
          var money = title === "count";
          if(money){
            return _getNumber(report[key][title], 0);
          }else{
            return _getNumber(report[key][title], 2) + " $";
          }
        }));
      });
    }

    function _processStatisticsCount(report, statistic){
      _processStatisticsCountGeneric(report, statistic.count, "count");
      _processStatisticsCountGeneric(report, statistic.amount, "sumAmount");
    }
    function _processStatisticsCountGeneric(report, statistic, attribute){
      var keys = ["legal", "fraud"];
      keys.forEach(function(key){
        statistic.labels.push(types[key]);
        statistic.data.push(_getNumber(report[key][attribute]));
      });
    }

    function _processStatisticsForeing(report, statistic){
      _processStatisticsYesNo(report, statistic.amount, "amountPerForeign", ["No extranjera", "Extranjera"]);
      _processStatisticsYesNo(report, statistic.count, "countPerForeign", ["No extranjera", "Extranjera"]);
    }

    function _processStatisticsEcommerce(report, statistic){
      _processStatisticsYesNo(report, statistic.amount, "amountPerEcommerce", ["No ecommerce", "Ecommerce"]);
      _processStatisticsYesNo(report, statistic.count, "countPerEcommerce", ["No ecommerce", "Ecommerce"]);
    }

    function _processStatisticsYesNo(report, statistic, attribute, labels){
      var keys = ["fraud", "legal"];
      statistic.labels = labels;
      keys.forEach(function(key){
        statistic.series.push(types[key]);
        statistic.data.push([
          ~~_getNumber(report[key][attribute][0]),
          ~~_getNumber(report[key][attribute][1])
        ]);
      });
    }

    function _processStatisticsAmountPerCountry(report, statistic){
      _processStatisticsKeyAndSerie(report, statistic.amount, "amountPerCountry", COUNTRY_CODE);
      _processStatisticsKeyAndSerie(report, statistic.count, "countPerCountry", COUNTRY_CODE);
    }

    function _processStatisticsAmountPerMerchant(report, statistic){
      _processStatisticsKeyAndSerie(report, statistic.amount, "amountPerMerchant", MERCHANT_CODE);
      _processStatisticsKeyAndSerie(report, statistic.count, "countPerMerchant", MERCHANT_CODE);
    }

    function _processStatisticsKeyAndSerie(report, statistic, attribute, labelName){
      var keys = Object.keys(types).reverse();
      var series = Object.keys(report.all[attribute]);

      statistic.series = keys.map(function(key){return types[key];});
      statistic.labels = series.map(labelName instanceof Function ? labelName : function(serie){return serie});
      keys.forEach(function(key){
        var values = [];
        series.forEach(function(serie){
          values.push(~~_getNumber(report[key][attribute][serie]));
        });
        statistic.data.push(values);
      });

    }


    function _getNumber(number, decimals){
      if(typeof number === 'undefined'){
        return;
      }
      if(number.value){
        var number = number.value;
      }
      if(!isNaN(decimals)){
        return $filter('number')(Number(number), decimals);
      }else{
        return Number(number);
      }
    }

  }

})();
