"use strict";

(function(){
  angular
    .module('bd4bf')
    .factory('interceptorMocksService', interceptorMocksService);

  interceptorMocksService.$inject = ['mocksFactory'];

  function interceptorMocksService(mocksFactory){
    var interceptor = {
      response: response,
      responseError: responseError
    };

    return interceptor;

    function response(response) {
      return response;
    }

    function responseError(rejection) {
      switch(rejection.config.url){
        /*  GET   */ case "/bd4bf-api/load-statistics": rejection.data = mocksFactory.api.loadStatistics; break;
        /*  GET   */ case "/bd4bf-api/model-features": rejection.data = mocksFactory.api.modelFeatures; break;
        /*  GET   */ case "/bd4bf-api/training": rejection.data = null; break;
      }

      return rejection;
    }

  }

})();

