"use strict";

(function(){
  angular
    .module('bd4bf')
    .factory("statisticsService", statisticsService);

  statisticsService.$inject = ['$q', 'apiStatistics'];

  function statisticsService($q, apiStatistics){
    var service = {
      getStatisticsList: getStatisticsList,
      getStatistic: getStatistic,
      getModelFeaturesList: getModelFeaturesList,
      getModelFeature: getModelFeature,
      getStartTraining: getStartTraining
    };

    return service;

    function getStatisticsList() {
      var defer = $q.defer();

      apiStatistics.query({method: "load-statistics"},function(list){
        if(list.length === 0){
          defer.reject("No hay estadísticas");
        }else{
          defer.resolve(list);
        }
      });

      return defer.promise;
    }

    function getStatistic(id){
      var defer = $q.defer();

     getStatisticsList()
      .then(function(list){
        var statistic = list.filter(function(statistic){
          return ~~statistic.date === ~~id;
        }).pop();

        if(statistic){
          defer.resolve(statistic);
        }else{
          defer.reject("No existe esta estadística");
        }
      });

      return defer.promise;
    }

    function getModelFeaturesList(){
      var defer = $q.defer();
      apiStatistics.query({method: "model-features"},function(list){
        if(list.length === 0){
          defer.reject("No hay datos");
        }else{
          defer.resolve(list);
        }
      });
      return defer.promise;
    }

    function getModelFeature(id){
      var defer = $q.defer();

     getModelFeaturesList()
      .then(function(list){
        var statistic = list.filter(function(statistic){
          return ~~statistic.date === ~~id;
        }).pop();

        if(statistic){
          defer.resolve(statistic);
        }else{
          defer.reject("No existe esta estadística");
        }
      });

      return defer.promise;
    }

    function getStartTraining(){
      return apiStatistics.get({method: "training"}).$promise;
    }
  }

})();
