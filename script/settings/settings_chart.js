"use strict";

(function(){
  angular
    .module('bd4bf')
    .run(chartSettings);

  function chartSettings(){
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = true;
    Chart.defaults.global.multiTooltipTemplate = '<%if (datasetLabel){%> <%=datasetLabel%>: <%}%><%= value %> ';
    Chart.defaults.global.tooltipTitleFontFamily = 'monospace';
    Chart.defaults.global.tooltipFontFamily = 'monospace';
    Chart.defaults.global.scaleFontFamily = 'monospace';
    Chart.defaults.global.tooltipFontSize = 12;
    Chart.defaults.Doughnut.animationEasing = "easeOutExpo";
    Chart.defaults.Bar.legendTemplate = Chart.defaults.Radar.legendTemplate;
    Chart.defaults.StackedBar.legendTemplate = Chart.defaults.Radar.legendTemplate;
    Chart.defaults.global.colours = ["#ffa300", "#ff7900", "#bac8ce"];
    //Chart.defaults.StackedBar.scaleLabel = "<%= Number(value / 1000000).toFixed(1).replace('.', ',') + ' M$'%>";
  }

})();
