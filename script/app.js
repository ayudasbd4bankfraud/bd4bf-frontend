"use strict";

(function(){

  angular
    .module('bd4bf', ['ngRoute', 'ui.router', 'ngResource', 'StratioUI', 'chart.js', 'ngFileUpload'])
    .constant("DEBUG", false)
    .config(routeConfiguration)
    .config(includeInterceptors)
    .run(function ($state, $rootScope) {
      $rootScope.$state = $state;
      $rootScope.Math = Math;
    })
    .filter("translate", function(){return function(input){return input;}});

  routeConfiguration.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  function routeConfiguration($stateProvider, $urlRouterProvider, $locationProvider) {
    //$locationProvider.html5Mode(true).hashPrefix('!');

    $urlRouterProvider.otherwise('/report');

    $stateProvider
      .state('report', {
        url: '/reporte',
        controller: 'reportsOverviewCtrl',
        controllerAs: 'vm',
        templateUrl: 'views/reports-overview.html'
      })
      .state('reportDetails', {
        url: '/reporte/:id',
        controller: 'reportDetailsCtrl',
        controllerAs: 'vm',
        templateUrl: 'views/report-details.html'
      })
      .state('training', {
        url: '/entrenamiento',
        controller: 'trainingCtrl',
        controllerAs: 'vm',
        templateUrl: 'views/model-features.html'
      })
  }

  includeInterceptors.$inject = ['$httpProvider', 'DEBUG']

  function includeInterceptors($httpProvider, DEBUG){
    if(DEBUG){
      $httpProvider.interceptors.push('interceptorMocksService');
    }
  }

})();
