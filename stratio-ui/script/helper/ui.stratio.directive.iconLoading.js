(function($){

'use strict';

/**
 *	@attribute st-icon-loading (1 >= x >= 0) : is a determinated loading, and set te loading bar with this percent
 *	@attribute st-icon-loading (x < 0) : is a indeterminated loading
 *	@attribute st-icon-loading (x === false) : hide the loading bar
 *	@exemple <ANY st-icon-loading="loadingPercent"></ANY>
 */

angular
	.module('StratioUI.directive.iconLoading', [])
	.directive('stIconLoading', stIconLoading);

function stIconLoading(){
	var className = "st-icon-loading";

	var directive = {
		restrict: 'A',
		scope: false,
		link: link
	};

	return directive;

	function link(scope, elem, attrs){

		var showBar = showBar;
		var getRealRotationFromMatrix = getRealRotationFromMatrix;

		var svgElement = $("<svg class='" + className + "'><circle></circle></svg>");
		var circle = svgElement.children();
		var circleWidth;
		var diameter;
		var perimeter;

		elem.append(svgElement);

		// Checking compatibility
		if(!('r' in document.body.style)){
			circle.attr({
				cx: 28,
				cy: 28,
				r: 23
			});
		}

		circleWidth = circle.get(0).getBBox().width;
		diameter = circleWidth;
		perimeter = diameter * Math.PI;


		if(circleWidth === 0){
			return;
		}

		elem.addClass(className + '-wrapper');
		showBar(false);

		scope.$watch(function(){return attrs.stIconLoading}, function(newValue, oldValue) {
			var percent = Number(newValue);

			if(newValue === 'false'){
				if(!('r' in document.body.style)){
					setProgress(0);
				}

				showBar(false);

			}else if(newValue !== '' && percent >= 0 && percent <= 1){
				setProgress(percent)
				showBar(true, true);
				
			}else if(newValue !== '' && percent < 0){
				setProgress(null)
				showBar(true, false);

			}
		});

		function showBar(show, isDeterminate){
			// jQuery's addClass / toggleClass don't work with SVG element
			svgElement.get(0).classList.toggle(className + '--hidden', show === false);

			if(typeof isDeterminate === 'boolean'){
				var matrix = window.getComputedStyle(svgElement.get(0)).transform;
				var realRotation = getRealRotationFromMatrix(matrix);
				var negativeRotation = realRotation

				if(negativeRotation > 0){
					negativeRotation -= 360;
				}


				setTimeout(function(){
					// Set in the circle the animation rotation for a correct transition
					svgElement.get(0).classList.toggle(className + '--indeterminate', !isDeterminate);

					svgElement.css("transition", "none");
					svgElement.css("transform", "rotate(" + (negativeRotation) + "deg)");
				}, 10);
				setTimeout(function(){
					svgElement.css("transition", "all 0.3s ease");
				}, 20);
				setTimeout(function(){
					svgElement.css("transform", "rotate(0deg)");
				}, 30);
				setTimeout(function(){
					svgElement.get(0).classList.toggle(className + '--determinate', isDeterminate);
				}, 500);
			}
		}

		function setProgress(percent){
			if(percent !== null){
				circle.css('stroke-dashoffset', (perimeter - (perimeter * percent)) + 'px');
			}else{
				circle.css('stroke-dashoffset', '');
			}
		}

		function getRealRotationFromMatrix(matrix){
			var matrixRotation = matrix.replace(/matrix\((-?[01](?:\.[0-9]{1,})?), (-?[01](?:\.[0-9]{1,})?),.+/, '$1/$2').split('/');
			var a = matrixRotation[0];
			var b = matrixRotation[1];

			var scale = Math.sqrt(a*a + b*b);
			var sin = b/scale;
			var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));

			return angle;
		}

	}
}

})(jQuery);