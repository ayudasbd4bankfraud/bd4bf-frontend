(function($){
'use strict';

/**
 * @attribute dynamic-form JSON array of scope path to get variable
 * @example	<ng-form name="form_$0_$1" dynamic-form='[["foo","bar"],["name"]]'> [...]
 */

angular
	.module('StratioUI.directive.dynamicForm', [])
	.directive('dynamicForm', dynamicForm);

function dynamicForm(){
	var directive = {
		restrict: 'A',
		priority: 0,
		require: ['form'],
		compile: compile
	};

	return directive;

	function compile(){
		return { pre:
			function(scope, element, attributes, ctrls) {
				var parentForm = $(element).parent().controller('form');
				if (parentForm) {
					var formCtrl = ctrls[0];

					delete parentForm[formCtrl.$name];

					formCtrl.$name = processName(scope, formCtrl.$name, attributes.dynamicForm);
					parentForm[formCtrl.$name] = formCtrl;
				}
			}
		}

		function processName(scope, name, vars){
			var variables = angular.fromJson(vars);
			var varContent = [];

			angular.forEach(variables, function(param){
				var result = scope;

				angular.forEach(param, function(variable){
					result = result[variable];
				});

				varContent.push(result)
			});

			angular.forEach(varContent, function(value, key){
				name = name.replace('$'+key, value);
			});

			return name;
		}
	}
};

})(jQuery);