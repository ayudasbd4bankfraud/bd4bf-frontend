'use strict';

(function(){

angular
	.module('StratioUI.helper.filter.inputSelect', [])
	.filter('stFilterSelect', stFilterSelect);

function stFilterSelect(){

	function filter(input, optionList, search){
		if(!input){
			return [];
		}
		return input.filter(function(optionKey){
			return optionList[optionKey].name.toLowerCase().indexOf((search || '').toLowerCase()) != -1;
		});
	}

	return filter;
}

})();


