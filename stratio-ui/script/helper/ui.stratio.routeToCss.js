(function($){




'use strict';

angular
	.module('StratioUI.helper.routeToCSS', [])
	.factory('stRouteToCSS', stRouteToCSS);

stRouteToCSS.$inject = ['$rootScope', '$state'];

function stRouteToCSS($rootScope, $state){
	$rootScope.$on('$stateChangeStart', setRouteToCSS);

	setTimeout(function(){
		setRouteToCSS(null, $state.$current);
	}, 10);

	var oldClasses = '';

	function setRouteToCSS(event, toState, toParams, fromState, fromParams){
		var sref = toState.name.split('.');
		var className = '';
		var $body = $('html');

		$body.removeClass(oldClasses);

		for(var i in sref){
			className += sref.slice(0, (~~i) + 1).join('_') + ' ';
		}

		oldClasses = className;

		$body.addClass(className);
	}

	return {};
}

})(jQuery);