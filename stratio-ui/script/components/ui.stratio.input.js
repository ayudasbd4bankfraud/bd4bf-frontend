'use strict';

/**
 * @example <st-input placeholder="Placeholder" ng-model="foo"></st-input>
 */

angular
	.module('StratioUI.components.input', [])
	.directive('stInput', stInput);

stInput.$inject = ['TEMPLATE_URL'];
function stInput(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			'ngModel': '=',
			'placeholder': '@',
			'type': '@',
			'required': '@',
			'min': '@',
			'max': '@',
			'autocomplete': '@',
			'errorMessage': '@',
			'decimals': '@',
			'initialValidation': '@',
			'variant': '@',
			'error': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'input'),
		transclude: true,
		replace: true,
		controller: controller,
		link: link
	};

	controller.$inject = ['$scope', '$element'];

	return directive;

	function controller($scope, $element){
		$scope.inputValue = String($scope.ngModel) && typeof $scope.ngModel != 'undefined' ? String($scope.ngModel) : '';
		
		$element.find('input').on('change', function() {
			$scope.inputValue = $element.find('input').val();
			$scope.$apply();
		});
	}

	function link(scope, elem, attrs){
		try{
			scope.$parent.$watch(attrs.ngDisabled, function(newVal){
		        scope.disabled = !!newVal;
		    });
		}catch(e){}
	}
}
