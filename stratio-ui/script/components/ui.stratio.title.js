'use strict';

angular
	.module('StratioUI.components.title', [])
	.directive('stTitle', stTitle);

stTitle.$inject = ['TEMPLATE_URL', 'stCloneTransclude'];
function stTitle(TEMPLATE_URL, stCloneTransclude){
	var directive = {
		restrict: 'E',
		scope: {
			'classIcon': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'title'),
		transclude: true,
		replace: true,
		link: stCloneTransclude
	};

	return directive;
}
