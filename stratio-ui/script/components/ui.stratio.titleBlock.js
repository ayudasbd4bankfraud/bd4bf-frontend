'use strict';

/**
 * @example <st-title-block title="Title text" icon="icon-class"> <!-- transclude --> </st-title-block>
 */

angular
	.module('StratioUI.components.titleBlock', [])
	.directive('stTitleBlock', stTitleBlock);

stTitleBlock.$inject = ['TEMPLATE_URL', 'stCloneTransclude'];
function stTitleBlock(TEMPLATE_URL, stCloneTransclude){
	var directive = {
		restrict: 'E',
		scope: true,
		templateUrl: TEMPLATE_URL('components', 'titleBlock'),
		transclude: true,
		replace: true,
		link: stCloneTransclude
	};

	return directive;
}
