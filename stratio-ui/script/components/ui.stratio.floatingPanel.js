(function(){

'use strict';

angular
	.module('StratioUI.components.floatingPanel', [])
	.directive('stFloatingPanel', stFloatingPanel);

stFloatingPanel.$inject = ['TEMPLATE_URL', 'stToggleFloatingElement'];

function stFloatingPanel(TEMPLATE_URL, stToggleFloatingElement){
	var directive = {
		restrict: 'E',
		scope: {
			'toggleId': "@"
		},
		templateUrl: TEMPLATE_URL('components', 'floatingPanel'),
		transclude: true,
		replace: true,
		controller: stToggleFloatingElement
	};

	return directive;
}


})();