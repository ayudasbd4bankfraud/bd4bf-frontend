'use strict';

/**
* 
*/

angular
.module('StratioUI.components.input.select', [])
.directive('stInputSelect', stInputSelect);

stInputSelect.$inject = ['TEMPLATE_URL', 'stToggleFloatingElement'];
function stInputSelect(TEMPLATE_URL, stToggleFloatingElement){
	var directive = {
		restrict: 'E',
		scope: {
			'ngModel': '=',
			'placeholder': '@',
			'required': '@',
			'variant': '@',
			'options': '@',
			'ngChange': '&',
			'onlyIcons': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'input.select'),
		transclude: true,
		replace: true,
		link: link,
		controller: stToggleFloatingElement
	};

	return directive;

	function link(scope, elem, attrs, ctrl, transclude){
		var options = attrs.options ? angular.fromJson(attrs.options) : [];
		var htmlOptions = [];

		resetOptions();

		try{
			scope.$parent.$watch(attrs.ngDisabled, function(newVal){
		        scope.disabled = !!newVal;
		    });
		}catch(e){}
	    
		scope.$watch("options", function (newOptions) {
			if(newOptions)
				loadNewOptions(angular.fromJson(newOptions));
		});

		angular.forEach(options, function(option){
			addNewOption(option);
		});

		var transclusion = transclude(function(content){
			angular.forEach(content, function(element, key){
				if(element.tagName != 'OPTION')
					return;

				var opt = {
					name: element.innerHTML,
					value: element.value,
					classIcon: element.getAttribute('class-icon'),
					selected: element.hasAttribute('selected')
				};

				htmlOptions.push(opt);
				addNewOption(opt);
			});
		});

		function resetOptions(){
			scope.optionsList = {};
			scope.optionsCleaned = [];
			scope.optionsKeys = [];
		}

		function addNewOption(option, key){
			if(option.selected)
				scope.ngModel = option.value;
			
			scope.optionsList[option.value] = option;
			
			if(scope.optionsKeys.indexOf(option.value) == -1){
				scope.optionsKeys.push(option.value);
			}
		}

		function loadNewOptions(options){
			resetOptions();

			angular.forEach(htmlOptions, function(option){
				addNewOption(option);
			});
			angular.forEach(options, function(option){
				addNewOption(option);
			});
		}
	}

}