'use strict';

(function($){

angular
	.module('StratioUI.components.stepsLine', [])
	.directive('stStepsLine', stStepsLine);

stStepsLine.$inject = ['TEMPLATE_URL', 'stPassAllAttributes'];
function stStepsLine(TEMPLATE_URL, stPassAllAttributes){
	var currentClassName = "steps-line__step--current";

	var directive = {
		restrict: 'E',
		scope: true,
		templateUrl: TEMPLATE_URL('components', 'stepsLine'),
		transclude: true,
		replace: true,
		controller: controller
	};

	controller.$inject = ['$rootScope', '$element', '$state'];

	return directive;

	function controller($rootScope, $element, $state){
		$rootScope.$on('$stateChangeStart', checkStepByState);

		setTimeout(function(){
			checkStepByState(null, $state.$current);
		}, 10);

		function checkStepByState(event, toState, toParams, fromState, fromParams){
			var sref = toState.name;

			$($element)
				.find(".step")
				.removeClass(currentClassName);

			$($element)
				.find(".step[sref~='" + sref + "']")
				.addClass(currentClassName);
		}
	}

}

})(jQuery);