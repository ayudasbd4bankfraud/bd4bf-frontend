'use strict';

angular
	.module('StratioUI.components.infoBlock', [])
	.directive('stInfoBlock', stInfoBlock);

stInfoBlock.$inject = ['TEMPLATE_URL'];
function stInfoBlock(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: true,
		templateUrl: TEMPLATE_URL('components', 'infoBlock'),
		transclude: true,
		replace: true
	};

	return directive;
}
