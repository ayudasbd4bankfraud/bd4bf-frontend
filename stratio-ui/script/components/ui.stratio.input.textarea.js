'use strict';

/**
 * 
 */

angular
	.module('StratioUI.components.input.textarea', [])
	.directive('stInputTextarea', stInputTextarea);

stInputTextarea.$inject = ['TEMPLATE_URL'];
function stInputTextarea(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			'ngModel': '=',
			'placeholder': '@',
			'required': '@',
			'errorMessage': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'input.textarea'),
		transclude: true,
		replace: true
	};

	return directive;
}
