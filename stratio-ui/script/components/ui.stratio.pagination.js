'use strict';

/**
 * @example  <st-pagination page="currentPage" total-pages="totalPages"></st-pagination>
 */

angular
	.module('StratioUI.components.pagination', [])
	.directive('stPagination', stPagination);

stPagination.$inject = ['TEMPLATE_URL'];
function stPagination(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			"page": "=",
			"totalPages": "="
		},
		templateUrl: TEMPLATE_URL('components', 'pagination'),
		transclude: true,
		replace: true,
		link: link
	};

	return directive;

	function link(scope, elem, attrs){

		scope.setPage = setPage;
		scope.maxVisibles = 5; // From the middle

		scope.$watch(function(){
			return scope.totalPages;
		}, function(newValue){
			scope.numbers = [];
			console.log(newValue)
			while(newValue--){
				scope.numbers.push("");
			}
		})

		function setPage(page){
			scope.page = page;
		}
	}
}
